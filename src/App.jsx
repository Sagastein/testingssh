import "./App.css";
import React from 'react'
import Siderbar from "./Components/Siderbar";
const App = () => {
  return (
    <main className="h-screen w-full bg-gray-300">
      <Siderbar/>
      </main>
  )
}

export default App