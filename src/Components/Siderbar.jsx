import React from "react";
import { Link } from "react-router-dom"
import {BsHouse,BsSearch,BsPlusSquare,BsPersonCircle} from "react-icons/bs"
import {FaArchive,FaInstagram,FaFacebookMessenger,FaHeart,FaBars} from "react-icons/fa"
const Siderbar = () => {
     const menu = [
          {"icon": <BsHouse/>, "name": "Home"},
          {"icon": <BsSearch/>, "name": "Search"},
          {"icon": <FaArchive/>, "name": "Explore"},
          {"icon": <FaInstagram/>, "name": "Reel"},
          {"icon": <FaFacebookMessenger/>, "name": "messages"},
          {"icon": <FaHeart/>, "name": "Notifications"},
          {"icon": <BsPlusSquare/>, "name": "Create"},
          {"icon": <BsPersonCircle/>, "name": "Profile"},
          {"icon": <FaBars/>, "name": "More"},


     ]
  return (
    <aside className="w-72 h-full px-8 bg-white">
      <div className="grid border pt-8">
        <h1 className="text-3xl italic">Instagram</h1>
      </div>
      <section className="pt-8 gap-y-6 grid">
          {
               menu.map((item,index)=>(
                    <div key={index} className="flex cursor-pointer duration-200 group items-center gap-x-4 border">
                    <span className="text-2xl group-hover:scale-105">{item.icon}</span>
                    <Link className="text-2xl font-thin capitalize group-hover:font-normal" to="/">{item.name}</Link>
                 </div>   
               ))
          }
      </section>
    </aside>
  );
};

export default Siderbar;
